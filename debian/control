Source: electrum
Maintainer: Debian Cryptocoin Team <team+cryptocoin@tracker.debian.org>
Uploaders: Soren Stoutner <soren@debian.org>
Section: utils
Priority: optional
# libsecp256k1-dev is used to speed up elliptic curve operations.
# protobuf-compiler is used to compile paymentrequest_pb2.py.
Build-Depends: debhelper-compat (= 13),
               devscripts,
               dh-sequence-python3,
Build-Depends-Indep: libsecp256k1-dev,
                     protobuf-compiler,
                     pyqt5-dev-tools,
                     python3,
                     python3-aiohttp,
                     python3-aiohttp-socks (>= 0.8.4),
                     python3-aiorpcx,
                     python3-attr,
                     python3-bitstring,
                     python3-cbor2,
                     python3-certifi,
                     python3-cryptography,
                     python3-dnspython,
                     python3-jsonpatch,
                     python3-pbkdf2,
                     python3-protobuf (>= 3.20),
                     python3-pyaes,
                     python3-pycryptodome,
                     python3-pyqt5.qtmultimedia,
                     python3-pyqt6,
                     python3-pyqt6.qtmultimedia,
                     python3-pyqt6.qtquick,
                     python3-qrcode,
                     python3-serial,
                     python3-setuptools,
                     python3-pytest <!nocheck>
Standards-Version: 4.7.1
Vcs-Git: https://salsa.debian.org/cryptocoin-team/electrum.git
Vcs-Browser: https://salsa.debian.org/cryptocoin-team/electrum
Homepage: https://electrum.org/
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild

Package: electrum
Architecture: all
Depends: python3-electrum (= ${binary:Version}),
         python3-pyqt5,
         ${misc:Depends},
         ${python3:Depends}
Recommends: python3-pyqt6
# fonts-adobe-sourcesans3 provides SourceSans3, which replaces the excluded electrum/plugins/revealer/SourceSansPro-Bold.otf.
# fonts-dejavu-core provides the excluded electrum/plugins/revealer/DejaVuSansMono-Bold.ttf.
# python3-btchip can be useful for hardware wallets.
# trezor is used by Trezor hardware wallets.
Suggests: fonts-adobe-sourcesans3,
          fonts-dejavu-core,
          python3-btchip,
          trezor
Description: Easy to use Bitcoin client
 This package provides a lightweight Bitcoin client which protects
 you from losing your bitcoins in a backup mistake or computer
 failure. Also, Electrum does not require waiting time because it does
 not download the Bitcoin blockchain.
 .
 Features of Electrum:
 .
   * Instant on: Your client does not download the blockchain. It uses a
     network of specialized servers that index the blockchain.
   * Forgiving: Your wallet can be recovered from a secret seed.
   * Safe: Your seed and private keys are encrypted on your hard drive.
     They are never sent to the servers.
   * Low trust: Information received from the servers is verified using
     SPV. Servers are authenticated using SSL.
   * No downtimes: Your client is not tied to a particular server; it
     will switch instantly if your server is down.
   * Ubiquitous: You can use the same wallet on different computers, they
     will synchronize automatically.
   * Cold Storage: Sign transactions from a computer that is always
     offline. Broadcast them using a machine that does not have your keys.
   * Reachable: You can export your private keys into other Bitcoin
     clients.
   * Established: Electrum is open source and was first released in
     November 2011.

Package: python3-electrum
Architecture: all
Section: python
Depends: libjs-jquery,
         libjs-jquery-ui,
         libjs-jquery-ui-theme-ui-lightness,
         node-qrcode-generator,
         python3-cryptography,
         ${libdep},
         ${misc:Depends},
         ${python3:Depends}
# python3-cbor is required by the Jade plugin.  See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1062448.
# python3-matplotlib is required to plot the transaction history.
# python3-zbar handles barcode scanning.
Recommends: python3-cbor,
            python3-matplotlib,
            python3-zbar
# fonts-adobe-sourcesans3 provides SourceSans3, which replaces the excluded electrum/plugins/revealer/SourceSansPro-Bold.otf.
# fonts-dejavu-core provides the excluded electrum/plugins/revealer/DejaVuSansMono-Bold.ttf.
# python3-btchip can be useful for hardware wallets.
# trezor is used by Trezor hardware wallets.
Suggests: electrum,
          fonts-adobe-sourcesans3,
          fonts-dejavu-core,
          python3-btchip,
          trezor
Description: Easy to use Bitcoin client - Python module
 This package provides a lightweight Bitcoin client which protects
 you from losing your bitcoins in a backup mistake or computer
 failure. Also, Electrum does not require waiting time because it does
 not download the Bitcoin blockchain.
 .
 This package provides the "electrum" Python module which can be used to access
 a Bitcoin wallet from Python programs.
